CREATE DATABASE examen;

USE examen;

CREATE TABLE examen(
	id_ex INT PRIMARY KEY AUTO_INCREMENT,
    name_ex VARCHAR(150)
);

CREATE TABLE eleve(
	id_e INT PRIMARY KEY AUTO_INCREMENT,
    firstname_e VARCHAR(100),
    lastname_e VARCHAR(100),
    date_n_e DATE,
    id_ex INT,
    FOREIGN KEY (id_ex) REFERENCES examen (id_ex)
);

CREATE TABLE dossier(
	num_do INT PRIMARY KEY AUTO_INCREMENT,
    id_e INT,
    FOREIGN KEY (id_e) REFERENCES eleve(id_e)
);

CREATE TABLE etablissement(
	id_et INT PRIMARY KEY AUTO_INCREMENT,
    name_et VARCHAR(200),
    adress_et VARCHAR(500),
    zip_et VARCHAR(5),
    city VARCHAR(100)
);

CREATE TABLE enseignant(
	matricule_en INT PRIMARY KEY AUTO_INCREMENT,
    firstname_en VARCHAR(100),
    lastname_en VARCHAR(100),
    tel_en VARCHAR(15),
    adress_en VARCHAR(500),
    zip_en VARCHAR(5),
    city_en VARCHAR (100),
    id_et INT,
    FOREIGN KEY (id_et) REFERENCES etablissement(id_et)
);

CREATE TABLE epreuve(
	id_ep INT PRIMARY KEY AUTO_INCREMENT,
    coef_ep INT,
    name_ep VARCHAR(100)
);

CREATE TABLE composition_examen (
    id_ex INT NOT NULL,
    id_ep INT NOT NULL,
    date_ep DATE,
    FOREIGN KEY (id_ex)
        REFERENCES examen (id_ex),
    FOREIGN KEY (id_ep)
        REFERENCES epreuve (id_ep),
    PRIMARY KEY (id_ex , id_ep)
);

CREATE TABLE enseignant_epreuve (
    matricule_en INT NOT NULL,
    id_ep INT NOT NULL,
    correction BOOLEAN,
    redaction BOOLEAN,
    FOREIGN KEY (matricule_en)
        REFERENCES enseignant (matricule_en),
    FOREIGN KEY (id_ep)
        REFERENCES epreuve (id_ep),
    PRIMARY KEY (matricule_en , id_ep)
);    

CREATE TABLE detail_note (
    id_e INT NOT NULL,
    id_ep INT NOT NULL,
    note FLOAT,
    FOREIGN KEY (id_e)
        REFERENCES eleve (id_e),
    FOREIGN KEY (id_ep)
        REFERENCES epreuve (id_ep),
    PRIMARY KEY (id_e , id_ep)
);